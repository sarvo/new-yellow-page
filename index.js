// const { PubSub, GraphQLServer } = require("graphql-yoga");
const express = require('express');
const {ApolloServer,PubSub,} = require("apollo-server-express")
const {mergeResolvers, mergeTypeDefs} = require('graphql-tools');
const {initialiseGQ} = require("./helper/gqlRegistration")
const mongoose = require("mongoose");
const dotenv = require('dotenv');
dotenv.config();
const {verifyJWT}= require("./helper/authMiddleware")


const { DB_URL } = process.env;

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true
});

const pubsub = new PubSub();

const initialiseServer = async()=>{
  const data = await initialiseGQ()
  const server = new ApolloServer({ typeDefs:mergeTypeDefs(data.registerTypes), resolvers: mergeResolvers(data.registerResolvers),
    context:  async({req}) =>{
        if (req.headers['authorization']) {
          let  data  = await verifyJWT(req)
          return { email:data.email };
        }else{
          return {}
        }
    }
  });
  
  const app = express();
  server.applyMiddleware({ app });
  
  app.listen({ port: process.env.PORT  }, () =>
    console.log(` Server ready at http://localhost:4000${server.graphqlPath}`)
  );
  
}
initialiseServer()

