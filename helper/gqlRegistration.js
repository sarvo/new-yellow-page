const { mergeResolvers, mergeTypeDefs } = require('graphql-tools');
const glob = require('glob')
module.exports = {
 initialiseGQ: async () => {

  let resolvers = glob.sync('graphql/*/*/*-Resolver.js')
  let registerResolvers = [];
  for (const r of resolvers) {
   console.log("r  ", r)
   // add resolvers to array
   registerResolvers = [...registerResolvers, require('../' + r),]
  }

  //iterate through resolvers file in the folder "graphql/folder/folder/whatever*-type.js"
  let types = glob.sync('graphql/*/*/*-Def.js')
  console.log("types",types)
  let registerTypes = [];
  for (const type of types) {
   console.log("Type ", type)
   // add types to array
   registerTypes = [...registerTypes, require('../' + type),]
  }
  return { registerResolvers, registerTypes }
 }
}