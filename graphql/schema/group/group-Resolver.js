const { Message,Group } = require('../../../models');

const groupResolver = {

Query: {
 group: async (_, { pageNum, pageSize }) => {
  pageNum = pageNum || 1;
  pageSize = pageSize || 100;
  const skips = pageSize * (pageNum - 1);
  const groupInfo = Group.find().skip(skips).limit(pageSize)
  return groupInfo
},

getGroupById: async (_, { groupName }, context) => {
  let data = await Group.find({ groupName })
  return data[0]
},
getGroupMessages: async (_, { groupName, pageNum, pageSize }, { email }) => {
  pageNum = pageNum || 1;
  pageSize = pageSize || 100;
  const skips = pageSize * (pageNum - 1);
  if (!email) {
    throw ("Not authorised")
  }
  const groupData = await Group.find({ groupName })

  if (groupData[0].emails.includes(email)) {
    const messages = await Message.find({ receiverMail: groupName }).skip(skips).limit(parseInt(pageSize)).sort({ timestamp: -1 })
    return messages
  }
} },

Mutation: {

 createGroup: async (_, { groupName: groupPublicName, emails }) => {
  let groupName = groupPublicName.split("").join("")
  let groupExist = await Group.find({ groupName })
  console.log(groupExist)
  if (groupExist.length) {
    throw Error("group exist")
  }
  if (!emails || !emails.length) {
    return ({ ok: false, status: 500, result: "please provide email in array" })
  }
  const newGroup = new Group({
    groupName,
    groupPublicName,
    emails
  })
  const createdGroup = await newGroup.save();
  return ({ ok: true, status: 200, result: JSON.stringify(newGroup) })
},


createGroupMessage: async (
  _,
  { receiverMail, message }, {email}
) => {
  // if (!context.email) return null;
  const groupInfo = await Group.find({groupName:receiverMail})
  if(!groupInfo || groupInfo.length <=0){
    return 
  }
  if (!groupInfo[0].emails.includes(email)) {
    return
  }
  const userText = new Message({
    senderMail:email,
    receiverMail,
    message,
    timestamp: new Date()
  });
  await userText.save();
  pubsub.publish("newMessage", {
    newMessage: userText,
    receiverMail
  });
  return userText;
},
}
}

module.exports =groupResolver