const { gql } = require('apollo-server-express')

const groupSchema = gql`
type Group {
 emails:[String],
 groupPublicName: String,
}

type Query {
 group(pageNum: Float, pageSize: Float):[Group]
 getGroupById(groupName: String!): Group
 getGroupMessages(groupName: String!, pageNum: Float, pageSize: Float):[Message]
}

type Mutation {
 createGroup(emails: [String], groupName:String!): Status
 createGroupMessage(receiverMail: String! message: String!): Message
}
 `;
 module.exports = groupSchema