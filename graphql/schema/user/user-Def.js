const { gql } = require('apollo-server-express')

const userSchema = gql`
type User {
 id: ID!
 name: String!
 email: String!
 password:String!
 messages: [Message]
}

type Status {
 status: Float
 ok: String
 result: String
}

type Query {
 users: [User]
}

type Mutation {
 createUser(name: String! email: String!, password: String!): Status
 loginUser(email: String!, password:String): String
}
 `;
 module.exports = userSchema