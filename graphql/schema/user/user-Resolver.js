const { User, Message } = require('../../../models');
const { generateJWT } = require("../../../helper/authMiddleware");

const userResolver = {
 User: {
  messages: async ({ email }) => {
    return Message.find({ senderMail: email });
  }
},

Query: {
 users: () => {
   console.log("hi there")
   return User.find()
 },

},

Mutation: {
 createUser: async (_, { name, email, password }, context) => {
   if (!context.email) return ({status:500,result:"sdfdsaf",ok:false});

   const user = new User({ name, email, password });
   await user.save();
   pubsub.publish("newUser", { newUser: user });
   return ({ status: 500, result: JSON.stringify(user), ok: false });;
 },

 loginUser: async (_, { email, password }, context) => {
   const userDetails = await User.find({ email })
   if (!userDetails.length || !userDetails[0].password) {
     return ("User not found")
   }
   let token = generateJWT({ email: userDetails[0]['email'] })
   console.log("userDetails ", token)
   return JSON.stringify(token);
 },
}
}

module.exports =userResolver