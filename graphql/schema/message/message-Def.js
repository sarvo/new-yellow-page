const { gql } = require('apollo-server-express')

const messageSchema = gql`
type Message {
 id: ID!
 message: String!
 senderMail: String!
 receiverMail: String!
 timestamp: Float!
 users: [User]
}

type Query {
 messages: [Message],
}

type Mutation {
 createMessage(senderMail: String! receiverMail: String! message: String! ): Message!
}
 
 `;
 module.exports = messageSchema