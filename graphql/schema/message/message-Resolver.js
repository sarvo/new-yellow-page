const { User, Message } =require('../../../models');

const messageResolver = {
 Message: {
  users: async ({ senderMail }) => {
    return User.find({ email: senderMail });
  }
},

Query: {
 messages: () => Message.find(),
},

Mutation: {

 createMessage: async (
  _,
  { senderMail, receiverMail, message }, 
) => {
  // if (!context.email) return null;
  const userText = new Message({
    senderMail,
    receiverMail,
    message,
    timestamp: new Date()
  });
  await userText.save();
  pubsub.publish("newMessage", {
    newMessage: userText,
    receiverMail
  });
  return userText;
},
}
}

module.exports =messageResolver