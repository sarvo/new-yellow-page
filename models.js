const mongoose = require('mongoose');

const User = mongoose.model("User", {
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    index: { unique: true }
  },
  password:{
    type: String
  }
});

const Message = mongoose.model("Message", {
  message: String,
  senderMail: String,
  receiverMail: String,
  timestamp: Number
});

const Group = mongoose.model("Group", {
  emails: [String],
  groupName:{type:String,unique:true},
  groupPublicName :{type:String},
  timestamp: Number
});

module.exports = { User, Message, Group };